/**
 * Represents an RGBA color. Values should normally be in the range [0.0, 1.0].
 * 
 * @constructor
 * @param {Number} r - red value (default 0.0)
 * @param {Number} g - green value (default 0.0)
 * @param {Number} b - blue value (default 0.0)
 * @param {Number} a - alpha value (default 1.0)
 */
function Color(r, g, b, a)
{
	this.r = (r ? r : 0.0);
	this.g = (g ? g : 0.0);
	this.b = (b ? b : 0.0);
	this.a = (a ? a : 1.0);
}

/**
 * Interpolates a color value within a rectangle based on an
 * x, y offset within the rectangle and the colors of the corners.
 * @param {Number} x - offset from left side
 * @param {Number} y - offset from bottom
 * @param {Number} width - width of rectangle
 * @param {Number} height - height of rectangle
 * @param {Color[]} colors - 4-element array of Color objects representing
 *     colors of the four corners, ordered counterclockwise from lower left
 * @return {Color} interpolated color at offset (x, y)
 */
function findRGB(x, y, width, height, colors)
{
	// TODO
}


