//
// Same as TextureThreejs but demonstrates asynchronous
// loading of images.
//
// Basic texture mapping example in Three.js.  
//


// entry point when page is loaded
function main() {
  
  var ourCanvas = document.getElementById('theCanvas');
  var renderer = new THREE.WebGLRenderer({canvas: ourCanvas});
  renderer.setClearColor(0x00cccc);
  var scene = new THREE.Scene();
  
  // ortho args are left, right, top, bottom (backwards!), near, far
  var camera = new THREE.OrthographicCamera(-1, 1, 1, -1, -1, 1);
  
  camera.position.x = 0;
  camera.position.y = 0;
  camera.position.z = 1;

  // texture map
  var url = "../images/check64border.png";
  //var url = "../images/clover_really_small.jpg";
  
  var loader = new THREE.TextureLoader();
  loader.load( 
      // load() method arguments are:
      
      // 1) image url
      url, 
      
      // 2) callback executed when image is loaded. Here we 
      //    add the textured square to the scene
      function ( texture ) {
        var geometry = new THREE.PlaneGeometry(1, 1);
        var material = new THREE.MeshBasicMaterial( { map: texture } );
        var square = new THREE.Mesh(geometry, material);
        scene.add(square);

        // texture parameters.  See the Texture class for properties, and 
        // see "Textures" in the documentation for names of constants
        texture.magFilter = THREE.NearestFilter;
        texture.minFilter = THREE.NearestFilter;
        //texture.wrapS = THREE.ClampToEdgeWrapping;
        //texture.wrapT = THREE.ClampToEdgeWrapping;
      }
  
      // 3, 4) optional: progress and error callback functions
  );

  
  var animate = function() {
    requestAnimationFrame( animate ); 
    renderer.render(scene, camera);
  };
  
  // draw!
  animate();
  
}