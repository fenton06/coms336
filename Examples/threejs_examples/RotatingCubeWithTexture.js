//
// The spinning cube example with Phong shading in three.js,
// using a texture for the surface of the model.
// Depends on CameraControl.js.
//

var axis = 'z';
var paused = false;
var camera;

//translate keypress events to strings
//from http://javascript.info/tutorial/keyboard-events
function getChar(event) {
if (event.which == null) {
 return String.fromCharCode(event.keyCode) // IE
} else if (event.which!=0 && event.charCode!=0) {
 return String.fromCharCode(event.which)   // the rest
} else {
 return null // special key
}
}

function handleKeyPress(event)
{
  var ch = getChar(event);
  if (cameraControl(camera, ch)) return;
  
  switch(ch)
  {
  case ' ':
    paused = !paused;
    break;
  case 'x':
    axis = 'x';
    break;
  case 'y':
    axis = 'y';
    break;
  case 'z':
    axis = 'z';
    break;
  default:
    return;
  }
}

function start()
{
  window.onkeypress = handleKeyPress;

  var scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera( 30, 1.5, 0.1, 1000 );
  camera.position.set(0, 0, 5);
  camera.lookAt(new THREE.Vector3(0, 0, 0));
  
  var ourCanvas = document.getElementById('theCanvas');
  var renderer = new THREE.WebGLRenderer({canvas: ourCanvas});

  // Choose a geometry
  //var geometry = new THREE.PlaneGeometry(1, 1, 1);
  var geometry = new THREE.BoxGeometry( 1, 1, 1 );
  //var geometry = new THREE.SphereGeometry(1);
  
  var url = "../images/check64border.png";
  var loader = new THREE.TextureLoader();
  var texture = loader.load(url);
  
  // Choose a material
  //var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
  //var material = new THREE.MeshLambertMaterial( { color: 0x00ff00 } );
  var material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x222222, shininess: 50, map: texture } );

  // Create a mesh
  var cube = new THREE.Mesh( geometry, material );
  
  // Add it to the scene
  scene.add( cube );
  
  // Make some axes
  var material = new THREE.LineBasicMaterial({color: 0xff0000});
  var geometry = new THREE.Geometry();
  geometry.vertices.push(
    new THREE.Vector3( 0, 0, 0 ),
    new THREE.Vector3( 2, 0, 0 )
  );
  var line = new THREE.Line( geometry, material );
  scene.add( line );
  
  material = new THREE.LineBasicMaterial({color: 0x00ff00});
  geometry = new THREE.Geometry();
  geometry.vertices.push(
    new THREE.Vector3( 0, 0, 0 ),
    new THREE.Vector3( 0, 2, 0 )
  );
  line = new THREE.Line( geometry, material );
  scene.add( line );

  material = new THREE.LineBasicMaterial({color: 0x0000ff});
  geometry = new THREE.Geometry();
  geometry.vertices.push(
    new THREE.Vector3( 0, 0, 0 ),
    new THREE.Vector3( 0, 0, 2 )
  );
  line = new THREE.Line( geometry, material );
  scene.add( line );

  // Put a point light in the scene
  var light = new THREE.PointLight(0xffffff, 1.0);
  light.position.set(-2, 3, 5);
  scene.add(light);
  
  // Put in an ambient light
  light = new THREE.AmbientLight(0x555555);
  scene.add(light);

  var render = function () {
    renderer.render(scene, camera);
    var increment = 0.5 * Math.PI / 180.0;  // convert to radians
    if (!paused)
    {
    	  // extrinsic rotations
      var q;
      switch(axis)
      {
      case 'x':
        // create a quaternion representing a rotation about x axis
        q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(1, 0, 0),  increment);       
        break;
      case 'y':
        q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 1, 0),  increment);
        break;
      case 'z':
        q = new THREE.Quaternion().setFromAxisAngle(new THREE.Vector3(0, 0, 1),  increment);
        break;       
      default:
      }
      // left-multiply the cube's quaternion, and then set the new value
      cube.setRotationFromQuaternion(q.multiply(cube.quaternion))
      requestAnimationFrame( render );
    }
  };


  render();
}