
// Using a cube map to map a texture onto a model.  See below to select texture
// and model. 
//
// Edit geometry and images below.
// To make a skybox, choose a box geometry and use a set of 6 
// coordinated images.
//

var theModel;
var imageNames;

// choose a geometry
theModel = getModelData(new THREE.BoxGeometry(1, 1, 1));
//theModel = getModelData(new THREE.SphereGeometry(1));
//theModel = getModelData(new THREE.SphereGeometry(1, 48, 24));
////theModel = getModelData(new THREE.TorusKnotGeometry(1, .4, 128, 16));

// choose image file for texture, if we're not making a skybox
//var imageFilename = "../images/check64.png";
//var imageFilename = "../images/check64border.png";
//var imageFilename = "../images/clover.jpg";
//var imageFilename = "../images/brick.png";
//var imageFilename = "../images/marble.png";
//var imageFilename = "../images/steve.png";
//var imageFilename = "../images/tarnish.jpg";

// we can use the same image in all 6 directions...
//imageNames = [
//              imageFilename,
//              imageFilename,
//              imageFilename,
//              imageFilename,
//              imageFilename,
//              imageFilename,
//              ];

//// ... or use 6 different images to make a skybox
var path = "../images/park/";
////var path = "../images/skybox/";
imageNames = [
          path + "px.jpg",
          path + "nx.jpg",
          path + "py.jpg",
          path + "ny.jpg",
          path + "pz.jpg",
          path + "nz.jpg"
          ];


//given an instance of THREE.Geometry, returns an object
//containing raw data for vertices and normal vectors.
function getModelData(geom)
{
  var verticesArray = [];
  var normalsArray = [];
  var vertexNormalsArray = [];
  var reflectedNormalsArray = [];
  var count = 0;
  for (var f = 0; f < geom.faces.length; ++f)
  {
    var face = geom.faces[f];
    var v = geom.vertices[face.a];
    verticesArray.push(v.x);
    verticesArray.push(v.y);
    verticesArray.push(v.z);

    v = geom.vertices[face.b];
    verticesArray.push(v.x);
    verticesArray.push(v.y);
    verticesArray.push(v.z);

    v = geom.vertices[face.c];
    verticesArray.push(v.x);
    verticesArray.push(v.y);
    verticesArray.push(v.z);
    count += 3;

    var fn = face.normal;
    for (var i = 0; i < 3; ++i)
    {
      normalsArray.push(fn.x);
      normalsArray.push(fn.y);
      normalsArray.push(fn.z);
    }

    for (var i = 0; i < 3; ++i)
    {
      var vn = face.vertexNormals[i];
      vertexNormalsArray.push(vn.x);
      vertexNormalsArray.push(vn.y);
      vertexNormalsArray.push(vn.z);
    }

  }

//texture coords
//each element is an array of three Vector2
  var uvs = geom.faceVertexUvs[ 0 ];
  var texCoordArray = [];
  for (var a = 0; a < uvs.length; ++a)
  {
    for (var i = 0; i < 3; ++i)
    {
      var uv = uvs[a][i];
      texCoordArray.push(uv.x);
      texCoordArray.push(uv.y);
    }
  }

  return {
    numVertices: count,
    vertices: new Float32Array(verticesArray),
    normals: new Float32Array(normalsArray),
    vertexNormals: new Float32Array(vertexNormalsArray),
    reflectedNormals: new Float32Array(reflectedNormalsArray),
    texCoords: new Float32Array(texCoordArray)
  };
}


// A few global variables...

// the OpenGL context
var gl;

// the geometry
var theModel;

// handle to a buffer on the GPU
var vertexbuffer;

// handle to the compiled shader program on the GPU
var shader;

// handle to the texture object on the GPU
var textureHandle;

var model = new Matrix4();
var modelScale = new Matrix4();

var axis = 'x';
var paused = true;

//instead of view and projection matrices, use a Camera
var camera = new Camera(30, 1.5);

//translate keypress events to strings
//from http://javascript.info/tutorial/keyboard-events
function getChar(event) {
if (event.which == null) {
return String.fromCharCode(event.keyCode) // IE
} else if (event.which!=0 && event.charCode!=0) {
return String.fromCharCode(event.which)   // the rest
} else {
return null // special key
}
}

//handler for key press events will choose which axis to
//rotate around
function handleKeyPress(event)
{
  var ch = getChar(event);

//if it was a camera control key, handle it
  if (camera.keyControl(ch)) return;

  switch(ch)
  {
  case 'p':
    camera.setPosition(0, 0, 0);
    break;
  case ' ':
    paused = !paused;
    break;
  case 'x':
    axis = 'x';
    break;
  case 'y':
    axis = 'y';
    break;
  case 'z':
    axis = 'z';
    break;
  case 'o':
    model.setIdentity();
    axis = 'x';
    break;


  default:
    return;
  }
}



// code to actually render our geometry
function draw()
{
  // clear the framebuffer
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BIT);

  // bind the shader
  gl.useProgram(shader);
  
  // get the index for the a_Position attribute defined in the vertex shader
  var positionIndex = gl.getAttribLocation(shader, 'a_Position');
  gl.enableVertexAttribArray(positionIndex);
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexbuffer);
  gl.vertexAttribPointer(positionIndex, 3, gl.FLOAT, false, 0, 0);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  // set uniform in shader for projection * view * model transformation
  var projection = camera.getProjection();
  var view = camera.getView();
  var transform = new Matrix4().multiply(projection).multiply(view).multiply(model).multiply(modelScale);
  var transformLoc = gl.getUniformLocation(shader, "transform");
  gl.uniformMatrix4fv(transformLoc, false, transform.elements);
  
  // need to choose a texture unit, then bind the texture to TEXTURE_2D for that unit
  var textureUnit = 0;
  gl.activeTexture(gl.TEXTURE0 + textureUnit);
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, textureHandle);
  var loc = gl.getUniformLocation(shader, "sampler");
  
  // sampler value in shader is set to index for texture unit
  gl.uniform1i(loc, textureUnit);  
  
  // draw, specifying the type of primitive to assemble from the vertices
  //gl.drawArrays(gl.TRIANGLES, 0, cube.numPoints);
  gl.drawArrays(gl.TRIANGLES, 0, theModel.numVertices);
  
  // unbind shader and "disable" the attribute indices
  // (not really necessary when there is only one shader)
  gl.disableVertexAttribArray(positionIndex);
  gl.useProgram(null);

}


  
// entry point when page is loaded.  Wait for image to load before proceeding
function main() {
  var images = [];  
  loadNext(images, 0);  
}

// load all six images...
function loadNext(images, index)
{
  if (index == 6)
  {
    startForReal(images);
  }
  else
  {
    images[index] = new Image();
    images[index].onload = function() { loadNext(images, index + 1); };
    images[index].src = imageNames[index];
  }
}


function startForReal(images) {
	
  // key handler
  window.onkeypress = handleKeyPress;
  
  // retrieve <canvas> element
  var canvas = document.getElementById('theCanvas');

  // get the rendering context for WebGL, using the utility from the teal book
  gl = getWebGLContext(canvas);
  //gl = getWebGLContext(canvas, {"antialias": false});
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  
  // load and compile the shader pair, using utility from the teal book
  var vshaderSource = document.getElementById('vertexShader').textContent;
  var fshaderSource = document.getElementById('fragmentShader').textContent;
  if (!initShaders(gl, vshaderSource, fshaderSource)) {
    console.log('Failed to intialize shaders.');
    return;
  }
  
  // retain a handle to the shader program, then unbind it
  // (This looks odd, but the way initShaders works is that it "binds" the shader and
  // stores the handle in an extra property of the gl object.  That's ok, but will really
  // mess things up when we have more than one shader pair.)
  shader = gl.program;
  gl.useProgram(null);
  
  // request a handle for a chunk of GPU memory
  vertexbuffer = gl.createBuffer();
  if (!vertexbuffer) {
	  console.log('Failed to create the buffer object');
	  return;
  }

  
  // "bind" the buffer as the current array buffer
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexbuffer);
  
  // load our data onto the GPU (uses the currently bound buffer)
  //gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
  //gl.bufferData(gl.ARRAY_BUFFER, cube.vertices, gl.STATIC_DRAW);
  gl.bufferData(gl.ARRAY_BUFFER, theModel.vertices, gl.STATIC_DRAW);
  
  // specify a fill color for clearing the framebuffer
  gl.clearColor(0.0, 0.8, 0.8, 1.0);
  gl.enable(gl.DEPTH_TEST);

  // ask the GPU to create a texture object
  textureHandle = gl.createTexture();
  
  // choose a texture unit to use during setup, defaults to zero
  // (can use a different one when drawing)
  // max value is MAX_COMBINED_TEXTURE_IMAGE_UNITS
  gl.activeTexture(gl.TEXTURE0);
  
  // bind the texture
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, textureHandle);
  
  // load the image bytes to the currently bound texture
  var imageTargets = [
  gl.TEXTURE_CUBE_MAP_POSITIVE_X,
  gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
  gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
  gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
  gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
  gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
  ];

  for (var i = 0; i < 6; ++i)
  {
    gl.texImage2D(imageTargets[i], 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, images[i]);
  }

  // texture parameters are stored with the texture
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
  //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);  // default is REPEAT
  //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);  // default is REPEAT

  camera.position[0] = 2;
  camera.position[1] = 2;
  camera.position[2] = 5;
  camera.lookAt(0, 0, 0);
  
  // define an animation loop
  var animate = function() {
  draw();
  
  // increase the rotation by some amount, depending on the axis chosen
  var increment = 0.5;
  if (!paused)
  {
    switch(axis)
    {
    case 'x':
      model = new Matrix4().setRotate(increment, 1, 0, 0).multiply(model);
      axis = 'x';
      break;
    case 'y':
      axis = 'y';
      model = new Matrix4().setRotate(increment, 0, 1, 0).multiply(model);
      break;
    case 'z':
      axis = 'z';
      model = new Matrix4().setRotate(increment, 0, 0, 1).multiply(model);
      break;
    default:
    }
  }
  
  // request that the browser calls animate() again "as soon as it can"
    requestAnimationFrame(animate, canvas); 
  };
  
  // start drawing!
  animate(); 

  
}