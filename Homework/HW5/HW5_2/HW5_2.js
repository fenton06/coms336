var path = "images/park/";

var imageNames = [
    path + "px.jpg",
    path + "nx.jpg",
    path + "py.jpg",
    path + "ny.jpg",
    path + "pz.jpg",
    path + "nz.jpg"
];

var camera;

function getChar(event) {
    if (event.which === null) {
        return String.fromCharCode(event.keyCode) // IE
    } else if (event.which !== 0 && event.charCode !== 0) {
        return String.fromCharCode(event.which)   // the rest
    } else {
        return null // special key
    }
}

function handleKeyPress(event) {
    var ch = getChar(event);
    if (cameraControl(camera, ch)) {
        return;
    }
}

function loadModel(url, callback) {
    var objLoader = new THREE.OBJLoader();
    objLoader.load(url, callback);
}

function loadModels(urls, callback) {

    var models = [];
    var modelsToLoad = urls.length;

    // Called each time a model loads
    var onModelLoad = function (thisModel) {
        --modelsToLoad;
        models.push(thisModel.children[0].geometry);

        // If all models loaded call callback
        if(modelsToLoad === 0) {
            callback(models);
        }
    };

    for (var i = 0; i < modelsToLoad; i++) {
        loadModel(urls[i], onModelLoad);
    }
}

function start() {

    loadModels([
       "models/teapot.obj",
       "models/bunny.obj"
    ], startForReal);
}


// Animation vars
var bunnyTurnIncrement = 1.0;
var bunnyJumpIncrement = 0.05;
var torusTurnIncrement = 0.25;
var windmillTurnIncrement = 0.75;

// models[0] = teapot, models[1] = bunny
function startForReal(models) {
    window.onkeypress = handleKeyPress;

    var scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, 1.5, 0.1, 1000);
    camera.position.set(20, 10, 20);
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    var ourCanvas = document.getElementById('theCanvas');

    var renderer = new THREE.WebGLRenderer({canvas: ourCanvas});
    renderer.setClearColor(0xffffff);

    // this is too easy, don't need a mesh or anything
    scene.background = new THREE.CubeTextureLoader().load(imageNames);

    // load texture - need to set wrapping parameters to repeat
    var url = "images/check64.png";
    var loader = new THREE.TextureLoader();
    var texture = loader.load(url);
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;

    var material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x222222, shininess: 50, map: texture } );

    // Create a mesh
    var teapotModel = new THREE.Mesh(models[0], material);

    // Make a big teapot!
    models[0].computeBoundingSphere();
    var teapotScale = 10 / models[0].boundingSphere.radius;
    teapotModel.scale.set(teapotScale, teapotScale, teapotScale);

    // Set position
    teapotModel.position.set(20, 0, -20);

    // Add it to the scene
    scene.add(teapotModel);

    material = new THREE.MeshPhongMaterial( { color: 0xffa500, specular: 0x222222, shininess: 50} );

    var torusGeometry = new THREE.TorusKnotGeometry();

    // Create a mesh
    var torusModel = new THREE.Mesh(torusGeometry, material);

    // Make it yuuuuge!
    torusGeometry.computeBoundingSphere();
    var torusScale = 10 / torusGeometry.boundingSphere.radius;
    torusModel.scale.set(torusScale, torusScale, torusScale);

    // Set position
    torusModel.rotateY(45 * Math.PI/180);
    torusModel.position.set(-20, 0, 20);

    // Add it to the scene
    scene.add(torusModel);

    // Hierarchy
    // create a mama rabbit
    var bunny = models[1];

    material = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x222222, shininess: 50} );

    // mamaDummy is parent of mamaBunny and babiesDummy
    var mamaDummy = new THREE.Object3D();
    mamaDummy.position.set(-20, 0, -20);

    var mama = new THREE.Mesh(bunny, material);
    mama.scale.set(3, 3, 3);
    mamaDummy.add(mama);

    // babyDummy is parent of bunny babies
    var babyDummy = new THREE.Object3D();
    mamaDummy.add(babyDummy);

    // babies are based around center of mama
    babyDummy.position.set(0, -1.5, 0);

    var baby1 = new THREE.Mesh(bunny, material);
    babyDummy.add(baby1);
    baby1.scale.set(1, 1, 1);

    // put baby bunny in front of mama bunny
    baby1.rotateY(90 * Math.PI/180);
    baby1.position.set(-7, 0, 0);

    var baby2 = new THREE.Mesh(bunny, material);
    babyDummy.add(baby2);
    baby2.scale.set(1, 1, 1);

    // put baby bunny at left side of mama bunny
    baby2.rotateY(-90 * Math.PI/180);
    baby2.position.set(0, 0, 7);

    var baby3 = new THREE.Mesh(bunny, material);
    babyDummy.add(baby3);
    baby3.scale.set(1, 1, 1);

    // put baby bunny at back of mama bunny
    baby3.rotateY(-90 * Math.PI/180);
    baby3.position.set(7, 0, 0);

    var baby4 = new THREE.Mesh(bunny, material);
    babyDummy.add(baby4);
    baby4.scale.set(1, 1, 1);

    // put baby bunny at right side of mama bunny
    baby4.rotateY(0);
    baby4.position.set(0, 0, -7);

    scene.add(mamaDummy);

    // More hierarchy!
    // Lets add a windmill!
    var cube = new THREE.BoxGeometry(1, 1, 1);
    material = new THREE.MeshLambertMaterial( { color: 0x00ff00 } );

    var towerDummy = new THREE.Object3D();
    towerDummy.position.set(20, 0, 20);
    towerDummy.rotateY(-135 * Math.PI/180);

    var tower = new THREE.Mesh(cube, material);
    tower.scale.set(1.5, 15.0, 1.5);
    towerDummy.add(tower);

    var housingDummy = new THREE.Object3D();
    housingDummy.position.set(0.0, 8.5, 0.0);
    towerDummy.add(housingDummy);

    var housing = new THREE.Mesh(cube, material);
    housing.position.set(0.0, 0.0, 0.5);
    housing.scale.set(2.0, 2.0, 4.0);
    housingDummy.add(housing);

    var hubDummy = new THREE.Object3D();
    hubDummy.position.set(0.0, 0.0, 3.25);
    housingDummy.add(hubDummy);

    var hub = new THREE.Mesh(cube, material);
    hub.position.set(0.0, 0.0, 0.0);
    hub.scale.set(1.5, 1.5, 1.5);
    hubDummy.add(hub);

    var rotorDummy = new THREE.Object3D();
    rotorDummy.position.set(0.0, 0.0, 0.0);
    hubDummy.add(rotorDummy);

    var rotor1 = new THREE.Mesh(cube, material);
    rotor1.position.set(0.0, 4.25, 0.0);
    rotor1.scale.set(1.0, 7.0, 0.25);
    rotor1.rotateY(45 * Math.PI/180);
    rotorDummy.add(rotor1);

    var rotor2 = new THREE.Mesh(cube, material);
    rotor2.position.set(0.0, -4.25, 0.0);
    rotor2.scale.set(1.0, 7.0, 0.25);
    rotor2.rotateY(-45 * Math.PI/180);
    rotorDummy.add(rotor2);

    scene.add(towerDummy);

    // Let there be light!!!
    var light = new THREE.PointLight(0xffffff, 1.0);
    light.position.set(2, 3, 5);
    scene.add(light);

    light = new THREE.AmbientLight(0x555555);
    scene.add(light);


    var render = function () {
        requestAnimationFrame(render);
        renderer.render(scene, camera);

        // Bunny animation
        babyDummy.rotateY(bunnyTurnIncrement * Math.PI / 180.0);

        var height = babyDummy.position.y;
        var newHeight;

        if(height < -1.5) {
            bunnyJumpIncrement *= -1;
        } else if(height > 0.0){
            bunnyJumpIncrement *= -1;
        }

        newHeight = height + bunnyJumpIncrement;
        babyDummy.position.set(0, newHeight, 0);

        // Torus animation
        torusModel.rotateY(torusTurnIncrement * Math.PI / 180.0);

        // Windmill animation
        hubDummy.rotateZ(windmillTurnIncrement * Math.PI / 180.0);

    };

    render();
}