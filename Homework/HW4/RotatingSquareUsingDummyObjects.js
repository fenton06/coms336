//
// Similar to RotatingSquareUsingObject, but uses CS336Object
// and the built-in methods for setting position, applying rotations,
// and scaling.  To set things up so that child objects are rendered
// with respect to the parent object's position and rotation, but
// not their scale, we use "dummy" objects in the hierarchy.
//

// Raw data for some point positions - this will be a square, consisting
// of two triangles.  We provide two values per vertex for the x and y coordinates
// (z will be zero by default).

var numPoints = 6;
var vertices = new Float32Array([
    -0.5, -0.5,
    0.5, -0.5,
    0.5, 0.5,
    -0.5, -0.5,
    0.5, 0.5,
    -0.5, 0.5
]);

// A few global variables...

// the OpenGL context
var gl;

// handle to a buffer on the GPU
var vertexbuffer;

// handle to the compiled shader program on the GPU
var shader;

// code to actually render our geometry
function drawCube(transformation) {

    // bind the shader
    gl.useProgram(shader);

    // bind the buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexbuffer);

    // get the index for the a_Position attribute defined in the vertex shader
    var positionIndex = gl.getAttribLocation(shader, 'a_Position');
    if (positionIndex < 0) {
        console.log('Failed to get the storage location of a_Position');
        return;
    }

    // "enable" the a_position attribute
    gl.enableVertexAttribArray(positionIndex);

    // associate the data in the currently bound buffer with the a_position attribute
    // (The '2' specifies there are 2 floats per vertex in the buffer.  Don't worry about
    // the last three args just yet.)
    gl.vertexAttribPointer(positionIndex, 2, gl.FLOAT, false, 0, 0);

    // we can unbind the buffer now (not really necessary when there is only one buffer)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // set the value of the uniform variable in the shader and draw
    var transformLoc = gl.getUniformLocation(shader, "transform");
    gl.uniformMatrix4fv(transformLoc, false, transformation.elements);
    gl.drawArrays(gl.TRIANGLES, 0, numPoints);

    // unbind shader and "disable" the attribute indices
    // (not really necessary when there is only one shader)
    gl.disableVertexAttribArray(positionIndex);
    gl.useProgram(null);

}

function draw(root) {
    gl.clear(gl.COLOR_BUFFER_BIT);
    root.render(new Matrix4());
}


// entry point when page is loaded
function main() {

    // basically this function does setup that "should" only have to be done once,
    // while draw() does things that have to be repeated each time the canvas is
    // redrawn

    // retrieve <canvas> element
    var canvas = document.getElementById('theCanvas');

    // get the rendering context for WebGL, using the utility from the teal book
    gl = getWebGLContext(canvas, false);

    if (!gl) {
        console.log('Failed to get the rendering context for WebGL');
        return;
    }

    // load and compile the shader pair, using utility from the teal book
    var vshaderSource = document.getElementById('vertexShader').textContent;
    var fshaderSource = document.getElementById('fragmentShader').textContent;

    if (!initShaders(gl, vshaderSource, fshaderSource)) {
        console.log('Failed to intialize shaders.');
        return;
    }

    // retain a handle to the shader program, then unbind it
    // (This looks odd, but the way initShaders works is that it "binds" the shader and
    // stores the handle in an extra property of the gl object.  That's ok, but will really
    // mess things up when we have more than one shader pair.)
    shader = gl.program;
    gl.useProgram(null);

    // request a handle for a chunk of GPU memory
    vertexbuffer = gl.createBuffer();

    if (!vertexbuffer) {
        console.log('Failed to create the buffer object');
        return;
    }

    // "bind" the buffer as the current array buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexbuffer);

    // load our data onto the GPU (uses the currently bound buffer)
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

    // now that the buffer is filled with data, we can unbind it
    // (we still have the handle, so we can bind it again when needed)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // specify a fill color for clearing the framebuffer
    gl.clearColor(0.0, 0.8, 0.8, 1.0);

    // set up an animation loop
    var degrees = 0;
    var increment = 2;
    var innerDegrees = 0;
    var x = .75 * Math.cos(degrees * Math.PI / 180.0);
    var y = .75 * Math.sin(degrees * Math.PI / 180);

    var square1Dummy = new CS336Object();
    square1Dummy.setPosition(x, y, 0);

    var square1 = new CS336Object(drawCube);
    square1.setScale(.2, .4, 1);

    square1Dummy.addChild(square1);

    var square2Dummy = new CS336Object();
    square2Dummy.setPosition(0, .5, 0);

    square1Dummy.addChild(square2Dummy);

    var square2 = new CS336Object(drawCube);
    square2.setScale(.2, .2, 1);

    square2Dummy.addChild(square2);

    var square3 = new CS336Object(drawCube);
    square3.setPosition(x, y, 0);
    square3.setScale(.1, .1, 1);

    square2Dummy.addChild(square3);

    var animate = function () {

        degrees += increment;
        innerDegrees += -increment * 2;

        x = .75 * Math.cos(degrees * Math.PI / 180.0);
        y = .75 * Math.sin(degrees * Math.PI / 180);
        square1Dummy.setPosition(x, y, 0);
        square1Dummy.rotateZ(-increment * 2);

        square2.rotateZ(increment);

        x = .2 * Math.cos(10 * degrees * Math.PI / 180.0);
        y = .2 * Math.sin(10 * degrees * Math.PI / 180);
        square3.setPosition(x, y, 0);
        square3.rotateZ(increment * 10);

//    square1.positionMatrix = new Matrix4().setTranslate(x, y, 0).rotate(innerDegrees, 0, 0, 1);
//
//    x = .2 * Math.cos(10 * degrees * Math.PI / 180.0);
//    y = .2 * Math.sin(10 * degrees * Math.PI / 180);
//
//    square3.positionMatrix = new Matrix4().setTranslate(x, y, 0);

        draw(square1Dummy);

        // request that the browser calls animate() again "as soon as it can"
        requestAnimationFrame(animate, canvas);
    };

    // draw!
    animate();
}
